FROM codercom/enterprise-base:ubuntu
USER root
RUN apt-get update && \
    DEBIAN_FRONTEND="noninteractive" apt-get install -y apt-transport-https ca-certificates lsb-release gnupg2 zsh rsync fzf openssh-server && \
    curl -L https://golang.org/dl/go1.16.5.linux-amd64.tar.gz -o go.tgz && \
    tar -C /usr/local -xzf go.tgz && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.21.0/bin/linux/amd64/kubectl &&\
    chmod +x ./kubectl && sudo mv ./kubectl /usr/local/bin/kubectl && \
    curl -L https://get.helm.sh/helm-v3.6.3-linux-amd64.tar.gz -o helm.tgz && \
    tar -zxvf helm.tgz && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    helm repo add stable https://charts.helm.sh/stable && \
    echo "PATH=/usr/local/go/bin:\$PATH" >> /home/coder/.bashrc && \
    echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - && \
    curl -sL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | tee /etc/apt/trusted.gpg.d/microsoft.gpg > /dev/null && \
    curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
    export LSB_REPO=$(lsb_release -cs) && \
    echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $LSB_REPO main" | tee /etc/apt/sources.list.d/azure-cli.list && \
    echo "deb [arch=amd64] https://apt.releases.hashicorp.com $LSB_REPO main" | tee /etc/apt/sources.list.d/hashicorp.list && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip -q awscliv2.zip && \
    ./aws/install && \
    apt-get update && DEBIAN_FRONTEND="noninteractive" apt-get install -y google-cloud-sdk azure-cli waypoint terraform jq && \
    curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C . && \
    mv eksctl /usr/local/bin && \
    curl --silent --location https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv4.2.0/kustomize_v4.2.0_linux_amd64.tar.gz | tar xz -C . && \
    mv kustomize /usr/local/bin && \
    curl -L https://github.com/openshift/okd/releases/download/4.7.0-0.okd-2021-08-22-163618/openshift-client-linux-4.7.0-0.okd-2021-08-22-163618.tar.gz | tar xz -C . && \
    mv oc /usr/local/bin && \
    curl -L https://github.com/norwoodj/helm-docs/releases/download/v1.5.0/helm-docs_1.5.0_linux_amd64.deb -o helm-docs.deb && dpkg -i helm-docs.deb

RUN echo  "StreamLocalBindUnlink yes" >> /etc/ssh/sshd_config && \
    systemctl --global mask gpg-agent.service gpg-agent.socket gpg-agent-ssh.socket gpg-agent-extra.socket gpg-agent-browser.socket && \
    systemctl enable ssh

USER coder
