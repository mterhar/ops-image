# ops-image

Run this in coder to have access to all sorts of cloud CLIs and such. 

[![Open in Coder Enterprise](https://cdn.coder.com/embed-button.svg)](https://master.cdr.dev/environments/git?org=default&image=5fc52c89-fca8fd5cfd0661ba0447bd4a&tag=latest&service=gitlab&repo=git@gitlab.com:mterhar/ops-image.git)

## Add to Coder Enterprise Images

Since it's in my personal namespace, regardless of public or private it will require a personal access token. This won't be a problem if it's in a group namespace and/or self-hosted. 

## Create an environment based on the image

Doesn't require much resources. 1 core, 4 gb ram, 5 gb of storage.

## Setup the cloud services

Once the environment is up, jump into a terminal or open `code` and pull up a terminal in there. 

### gcloud 

`gcloud auth login --no-launch-browser` will give you a login URL which you can open in a browser. Then paste the code back in to authenticate. 

Adding a cluster, `gcloud container clusters get-credentials CLUSTER_NAME --zone ZONE_NAME --project PROJECT_ID`

Adds the new context to the kubeconfig and activates it. 

## Test `kubectl`

```
coder@operations:~/gcp-test-cluster$ kubectl version
Client Version: version.Info{Major:"1", Minor:"19", GitVersion:"v1.19.0", GitCommit:"e19964183377d0ec2052d1f1fa930c4d7575bd50", GitTreeState:"clean", BuildDate:"2020-08-26T14:30:33Z", GoVersion:"go1.15", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"17+", GitVersion:"v1.17.13-gke.2001", GitCommit:"00c919adfe4adf308bcd7c02838f2a1b60482f02", GitTreeState:"clean", BuildDate:"2020-11-06T18:24:02Z", GoVersion:"go1.13.15b4", Compiler:"gc", Platform:"linux/amd64"}
```

## Test `helm` 

[Install coder](https://enterprise.coder.com/docs/installation) as a test?

```bash
helm repo add coder https://helm.coder.com
helm search repo coder
kubectl create ns coder
helm install --namespace coder coder coder/coder --version 1.13.2
```